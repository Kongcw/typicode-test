package com.tribehired.typicode.web;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.tribehired.typicode.domain.dto.CommentDTO;
import com.tribehired.typicode.domain.dto.CommentRequest;
import com.tribehired.typicode.service.CommentService;
import com.tribehired.typicode.service.PostService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * MainDisplayResource
 */
@RestController
@RequestMapping("/api/public")
public class PublicGatewayResource {

    private final Logger log = LoggerFactory.getLogger(PublicGatewayResource.class);

    private CommentService commentService;
    private PostService postService;

    public PublicGatewayResource(CommentService commentService, PostService postService) {
        this.commentService = commentService;
        this.postService = postService;
    }

    @GetMapping("/comments")
    public ResponseEntity<?> getListOfComments(@RequestParam Map<String, String> allParams) {
        log.debug("getListOfComments request: {}", allParams);
        List<Map<String, String>> comments = commentService.getCommentList(allParams);
        
        return ResponseEntity.ok().body(comments);
    }

    @GetMapping("/topPosts")
    public ResponseEntity<?> getListOfTopPost() {
        log.debug("getListOfTopPost");
        return ResponseEntity.ok().body(postService.getTopPost());
    }
}