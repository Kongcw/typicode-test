package com.tribehired.typicode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TypiCodeApplication {

	public static void main(String[] args) {
		SpringApplication.run(TypiCodeApplication.class, args);
	}

}
