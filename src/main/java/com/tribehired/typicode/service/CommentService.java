package com.tribehired.typicode.service;

import java.util.List;
import java.util.Map;

import com.tribehired.typicode.domain.dto.CommentDTO;
import com.tribehired.typicode.domain.dto.CommentRequest;

public interface CommentService {

    public List<Map<String, String>> getCommentList(Map<String, String> request);

}