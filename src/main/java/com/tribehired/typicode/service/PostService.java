package com.tribehired.typicode.service;

import java.util.List;

import com.tribehired.typicode.domain.dto.PostDTO;
import com.tribehired.typicode.domain.dto.TopPostResponse;

public interface PostService {

    public List<PostDTO> getPostList();

    public List<TopPostResponse> getTopPost();

}