package com.tribehired.typicode.service.impl;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.tribehired.typicode.common.http.HttpClient;
import com.tribehired.typicode.domain.dto.CommentDTO;
import com.tribehired.typicode.domain.dto.CommentRequest;
import com.tribehired.typicode.service.CommentService;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class CommentServiceImpl implements CommentService {

    private String url = "https://jsonplaceholder.typicode.com/comments";
    private HttpClient client;

    public CommentServiceImpl() {
        this.client = new HttpClient();
    }

    @Override
    public List<Map<String, String>> getCommentList(Map<String, String> request) {
        ResponseEntity<List<Map<String, String>>> response = client.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<Map<String, String>>>() {
                });
        List<Map<String, String>> comments = response.getBody();
       if(request != null) {
           comments = comments.stream().filter((Map<String, String> c) -> {
               int count = 0;
               int mapsize = request.size();
               for(Map.Entry<String, String> entry: request.entrySet()) {
                   if(c.get(entry.getKey()).equals(entry.getValue())) {
                       count ++;
                   }
               }
   
               return mapsize == count;
            
           }).collect(Collectors.toList());
       }
        return comments;
    }

}