package com.tribehired.typicode.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.tribehired.typicode.common.http.HttpClient;
import com.tribehired.typicode.domain.dto.CommentDTO;
import com.tribehired.typicode.domain.dto.CommentRequest;
import com.tribehired.typicode.domain.dto.PostDTO;
import com.tribehired.typicode.domain.dto.TopPostResponse;
import com.tribehired.typicode.service.CommentService;
import com.tribehired.typicode.service.PostService;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class PostServiceImpl implements PostService {

    private String url = "https://jsonplaceholder.typicode.com/comments";
    private HttpClient client;

    private CommentService commentService;

    public PostServiceImpl(CommentService commentService) {
        client = new HttpClient();
        this.commentService = commentService;
    }

    @Override
    public List<PostDTO> getPostList() {
        ResponseEntity<List<PostDTO>> response = client.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<PostDTO>>() {
                });
        List<PostDTO> posts = response.getBody();

        return posts;
    }

    @Override
    public List<TopPostResponse> getTopPost() {
        ResponseEntity<List<PostDTO>> response = client.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<PostDTO>>() {
                });
        List<PostDTO> posts = response.getBody();

        Map<Integer, TopPostResponse> postResponsesMap = posts.stream().map(p -> {
            TopPostResponse postResponse = new TopPostResponse(p.getId(), p.getTitle(), p.getBody(), 0);
            return postResponse;
        }).collect(Collectors.toMap(TopPostResponse::getId, t -> t));
        List<Map<String, String>> comments = commentService.getCommentList(null);
       
        for (Map<String, String> commentDTO : comments) {
            TopPostResponse postResponse = postResponsesMap.get(Integer.parseInt(commentDTO.get("id")));
            if(postResponse != null) {
                postResponse.setTotalNumberOfComment(postResponse.getTotalNumberOfComment() + 1);
            }
        }
        ArrayList<TopPostResponse> topPostResponses = new ArrayList<>(postResponsesMap.values());
        Collections.sort(topPostResponses);
        return topPostResponses;

    }

}