package com.tribehired.typicode.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TopPostResponse implements Comparable<TopPostResponse> {
    @JsonProperty("post_id")
    private Integer id;
    @JsonProperty("post_title")
    private String postTitle;
    @JsonProperty("post_body")
    private String postBody;
    @JsonProperty("total_number_of_comments")
    private Integer totalNumberOfComment;

    public TopPostResponse(Integer id, String postTitle, String postBody, Integer totalNumberOfComment) {
        this.id = id;
        this.postTitle = postTitle;
        this.postBody = postBody;
        this.totalNumberOfComment = totalNumberOfComment;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTotalNumberOfComment() {
        return totalNumberOfComment;
    }

    public void setTotalNumberOfComment(Integer totalNumberOfComment) {
        this.totalNumberOfComment = totalNumberOfComment;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostBody() {
        return postBody;
    }

    public void setPostBody(String postBody) {
        this.postBody = postBody;
    }

    @Override
    public int compareTo(TopPostResponse comparestu) {
        int compareComments = ((TopPostResponse) comparestu).getTotalNumberOfComment();
        return compareComments - this.totalNumberOfComment;

    }

    @Override
    public String toString() {
        return "TopPostResponse [id=" + id + ", postBody=" + postBody + ", postTitle=" + postTitle
                + ", totalNumberOfComment=" + totalNumberOfComment + "]";
    }

}